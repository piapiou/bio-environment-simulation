using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PerlinParameters
{
    public float m_Amplitude;
    public float m_Frequency;
    public float m_Threshold;
}

public class PerlinNoise
{
    public PerlinParameters m_Parameters;
    // TODO : Add rotation;

    public Texture2D m_WhiteNoise;

    float easeFunction(float a_in)
    {
        a_in /= .5f;
        if (a_in < 1) return 0.5f * a_in * a_in;
        a_in--;
        return -1.0f * 0.5f * (a_in * (a_in - 2) - 1);
    }

    int TrueMod(int x, int m)
    {
        return (x % m + m) % m;
    }

    public float Evaluate(Vector2 a_Coordinate)
    {
        return ScaledEvaluate(a_Coordinate * m_Parameters.m_Frequency) * m_Parameters.m_Amplitude;
    }

    private float ScaledEvaluate(Vector2 a_Coordinate)
    {
        int whiteNoiseWidth = m_WhiteNoise.width;
        int whiteNoiseHeight = m_WhiteNoise.height;
 
        int lowX = TrueMod(Mathf.FloorToInt(a_Coordinate.x), whiteNoiseWidth);
        int lowY = TrueMod(Mathf.FloorToInt(a_Coordinate.y), whiteNoiseHeight);
        int highX = TrueMod(Mathf.CeilToInt(a_Coordinate.x), whiteNoiseWidth);
        int highY = TrueMod(Mathf.CeilToInt(a_Coordinate.y), whiteNoiseHeight);

        float decimalX = a_Coordinate.x - (int)a_Coordinate.x;
        decimalX = (a_Coordinate.x > 0) ? decimalX : 1 - decimalX;
        float decimalY = a_Coordinate.y - (int)a_Coordinate.y;
        decimalY = (a_Coordinate.y > 0) ? decimalY : 1 - decimalY;

        float p1Value = m_WhiteNoise.GetPixel(lowX, lowY  ).r * easeFunction(1.0f - decimalX) * easeFunction(1.0f - decimalY);
        float p2Value = m_WhiteNoise.GetPixel(lowX, highY ).r * easeFunction(1.0f - decimalX) * easeFunction(       decimalY);
        float p3Value = m_WhiteNoise.GetPixel(highX, lowY ).r * easeFunction(       decimalX) * easeFunction(1.0f - decimalY);
        float p4Value = m_WhiteNoise.GetPixel(highX, highY).r * easeFunction(       decimalX) * easeFunction(       decimalY);

        float calculatedValue = p1Value + p2Value + p3Value + p4Value;

        if (calculatedValue > m_Parameters.m_Threshold)
        {
            calculatedValue = m_Parameters.m_Threshold * 2 - calculatedValue;
        }

        return calculatedValue;
    }
}