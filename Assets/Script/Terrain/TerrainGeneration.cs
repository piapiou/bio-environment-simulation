using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGeneration : MonoBehaviour
{
    public Texture2D m_WhiteNoiseTexture;
    public List<PerlinParameters> m_PerlinsParameters;

    //-----------------------------

    private List<PerlinNoise> m_Perlin;
    public Terrain m_Terrain;

    //-----------------------------

    void Start()
    {
        m_Terrain = GetComponent<Terrain>();

        m_Perlin = new List<PerlinNoise>();
        foreach(PerlinParameters param in m_PerlinsParameters)
        {
            PerlinNoise pNoise = new PerlinNoise();
            pNoise.m_WhiteNoise = m_WhiteNoiseTexture;
            pNoise.m_Parameters = param;

            m_Perlin.Add(pNoise);
        }

        ResetTerrain();
        GenerateTerrain();
    }

    void ResetTerrain()
    {
        UnityEngine.TerrainData terrainData = m_Terrain.terrainData;
        int mapSize = terrainData.heightmapResolution;
        float[,] values = new float[mapSize, mapSize];
        for (int i = 0; i < mapSize; i++)
        {
            for (int j = 0; j < mapSize; j++)
            {
                values[i, j] = 0.0f;
            }
        }
        terrainData.SetHeights(0, 0, values);
    }

    void GenerateTerrain()
    {
        UnityEngine.TerrainData terrainData = m_Terrain.terrainData;
        int mapSize = terrainData.heightmapResolution;
        float maxHeight = terrainData.size.y;
        float[,] values = new float[mapSize, mapSize];
        for (int i = 0; i < mapSize; i++)
        {
            for (int j = 0; j < mapSize; j++)
            {
                float coordinateX = terrainData.size.x * ((float)i / (float)mapSize);
                float coordinateZ = terrainData.size.z * ((float)j / (float)mapSize);
                float pointVal = 0;
                foreach (PerlinNoise param in m_Perlin)
                {
                    pointVal += param.Evaluate(new Vector2(coordinateX, coordinateZ));
                }
                values[i, j] = pointVal / maxHeight;
            }
        }
        terrainData.SetHeights(0, 0, values);
    }
}
